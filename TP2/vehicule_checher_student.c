#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>


int main(int argc, char **argv)
{
	int s; 
	struct sockaddr_can addr;
	struct ifreq ifr;

	printf("CAN Sockets Demo\r\n");

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "vcan0" );
	ioctl(s, SIOCGIFINDEX, &ifr);

	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}


    // Receive IDS 0xC00 to 0xC07

    struct can_filter rfilter[1];

    // We listen for every frames with an ID between 0x100 and 0x1FF so we use a filter 👂
	rfilter[0].can_id   = 0xC00;
	rfilter[0].can_mask = 0xFF8;

	setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));

    int nbytes;
	struct can_frame frame;

    for (int i=0; i<8; i++){
	    nbytes = read(s, &frame, sizeof(struct can_frame));
		if (nbytes < 0) {
			perror("Read");
			return 1;
		}
    }

	printf("SIM Started !\r\n");

	// SEND LIGHTS Commandes

	struct can_frame light_frame_cmd [5];

	for (int i = 0; i<sizeof(light_frame_cmd); i++) {
		light_frame_cmd[i].can_id = 0x123;
		light_frame_cmd[i].can_dlc = 2;
	}

	unsigned char ligh_cmd [10] = {0x00, 0x01, 0x00, 0x02, 0x01, 0x00, 0x02, 0x00, 0x00, 0x00};


	for (int i = 0; i < 5; i++) {
		sprintf(light_frame_cmd[i].data, "%c%c", ligh_cmd[2*i], ligh_cmd[2*i+1]);
		
		if (write(s, &light_frame_cmd[i], sizeof(struct can_frame)) != sizeof(struct can_frame)) {
            perror("Write");
            return 1;
        }

		sleep(3);
	}
	
	// SEND MOVES Commandes

	struct can_frame move_frame_cmd [4];

	for (int i = 0; i<sizeof(move_frame_cmd); i++) {
		move_frame_cmd[i].can_id = 0x321;
		move_frame_cmd[i].can_dlc = 3;
	}

	unsigned char move_cmd [12] = {0x64, 0x00, 0x09, 0x10, 0x00, 0x02, 0x10, 0x00, 0x00, 0x00, 0x20, 0x00};


	for (int i = 0; i < 4; i++) {
		sprintf(move_frame_cmd[i].data, "%c%c%c", move_cmd[3*i], move_cmd[3*i+1], move_cmd[3*i+2]);
		
		if (write(s, &move_frame_cmd[i], sizeof(struct can_frame)) != sizeof(struct can_frame)) {
            perror("Write");
            return 1;
        }

		sleep(6);
	}



    // CLOSE
	if (close(s) < 0) {
		perror("Close");
		return 1;
	}

	return 0;
}