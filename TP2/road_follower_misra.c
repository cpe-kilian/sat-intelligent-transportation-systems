/*
Speed: 27 km/h
Gear: 2
Motor speed: 2700 rpm
Action to follow the road: ->
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>


int main()
{
	int s; 
	struct sockaddr_can addr;
	struct ifreq ifr;

	(void) printf("CAN Sockets Demo\r\n");

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}

	(void) strcpy(ifr.ifr_name, "vcan0" );
	ioctl(s, SIOCGIFINDEX, &ifr);

	(void) memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}


    // Receive IDS 0xC00 to 0xC07

    struct can_filter rfilter[1];

    // We listen for every frames with an ID between 0x100 and 0x1FF so we use a filter 👂
	rfilter[0].can_id   = 0xC00;
	rfilter[0].can_mask = 0xFF8;

	setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));

    int nbytes;
	struct can_frame frame;

    float speed = 0;
    int camera_road_sensor [6] = {0, 0, 0, 0, 0, 0};

    int frame_id;

    int throttle = 0x00;
    int brake = 0x00;
    int steering = 0x00;
    int last_steering = 0x00;

    int speed_delta;

    while(1) {
        //##############################
        //# READ INCOMING DATA 🕶️      #
        //##############################
        nbytes = read(s, &frame, sizeof(struct can_frame));
        if (nbytes < 0) {
            perror("Read");
            return 1;
        }

        //##############################
        //# HANDLE FRAME 🕶️            #
        //##############################

        // We already use a filter to get 0xC00 to 0xC007 so we can focus on the last digit of the id now
        frame_id = frame.can_id%10;
        
        switch (frame_id) {
            case 0: // FULL LEFT
            case 1: // LEFT
            case 2: // MIDDLE LEFT
            case 3: // MIDDLE RIGHT
            case 4: // RIGHT
            case 5: // FULL RIGHT
                camera_road_sensor[frame_id] = frame.data[0];
                break;
            case 7: // SPEED frame
                speed = (double) frame.data[0] / 0xD6 * 214;
                break;
        }



        //#################################
        //#  SPEED ADJUSTMENT          🕶️ #
        //#################################

        speed_delta = 50 - speed;

        if (speed_delta > 0) {
            throttle = speed_delta * 2;
            brake = 0;
        } else {
            throttle = 0;
            brake = speed_delta * -1;
        }


        //#################################
        //#  Calcul  DIRECTION weights 🕶️ #
        //#################################
        
        int left_weight = (camera_road_sensor[0]*1.5) + camera_road_sensor[1];
        int middle_weight = (camera_road_sensor[2]*2.5) + (camera_road_sensor[3]*2.5);
        int right_weight = camera_road_sensor[4] + (camera_road_sensor[5]*1.5);

        char arrow;

        if (left_weight > middle_weight) {
            steering = 50;
        } else {
            steering = 0;
        }
        if ((right_weight > left_weight) && (right_weight > middle_weight)) {
            steering = -50;
        }


        //#################################
        //#  Send Instructions         🕶️ #
        //#################################
        frame.can_id = 0x321;
        frame.can_dlc = 3;
        (void) sprintf(frame.data, "%c%c%c", throttle, brake, steering);


        if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
            perror("Write");
            return 1;
        }



        // Temporisation si changement de direction pour eviter le zig zag

        if (steering != last_steering) {
            last_steering = steering;
            sleep(0.8);
        }

        
        (void) printf("%d %d %d %d %d %d\r\n", camera_road_sensor[0], camera_road_sensor[1], camera_road_sensor[2], camera_road_sensor[3], camera_road_sensor[4], camera_road_sensor[5]);

    }



    // CLOSE
	if (close(s) < 0) {
		perror("Close");
		return 1;
	}

	return 0;
}