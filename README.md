# Author
Kilian Decaderincourt
 
# Links
[Sujet tp](https://gitlab.com/m0rph03nix/sadt_student/-/wikis/home)

[Fichiers tp](https://gitlab.com/m0rph03nix/sadt_student)

# Setup

```bash
python3 -m virtualenv venv_SAdT --python=/usr/bin/python3.7

source venv_SAdT/bin/activate

pip install avsim2D
```

# TP
## TP1

### can-utils:
```bash
candump vcan0 # Print every frames received on vcan0
```

```bash
cansend vcan0 123#F0A1DD03 # Send 4 bytes 0xF0 0xA1 0xDD 0x03 with frame ID Ox123
```

### Reverse engineering

Avec un `candump` on voit beaucoup de frames passer, il y a des frames avec un id sur 29 bits toutes les secondes et des frames plus rare d'id `123` et `232`. On peut déjà suposer que les premiers sont des données des capteurs de la voitrue et les autres les instructions envoyés.

Pour y voir plus clair on passe un filtre au candump pour voir uniquement les instructions 

```bash
candump vcan0,100:100
```

On voit clairement les instructions et on peut les les rejouer avec un cansend ex:
```bash
cansend vcan0 323#0001
```

blink right blinkers: `123#0001`

blink left blinkers: `123#0002`

switch low beam on: `123#0100`

switch high beam on: `123#0200`

switch every beam off: `123#0000`

## TP2a
```
vcan0  00000C00   [8]  44 00 05 09 00 00 00 00

In the full left area (ID 0xC00), the vehicle sees :
- the road : 0x44
- yield marking : 0x05
- road crossing : 0x09
```
```
vcan0  00000C01   [8]  2B 00 00 09 00 00 00 0B

In the left area (ID 0xC01), the vehicle sees :
- road : 0x2B
- crossing : 0x09
- reserved? : 0x0B
```
```
vcan0  00000C02   [8]  33 00 00 06 00 00 00 0C

In the middle_left area (ID 0xC02), the vehicle sees :
- road : 0x33
- crossing : 0x06
- reserved? : 0x0C
```
```
vcan0  00000C03   [8]  0F 00 00 00 00 00 00 06

In the middle_right area (ID 0xC03), the vehicle sees :
- road : 0x0F
- reserved? : 0x06
```
```
vcan0  00000C04   [8]  06 00 00 00 00 00 00 00

In the right area (ID 0xC04), the vehicle sees :
- road : 0x06
```
```
vcan0  00000C05   [8]  2D 00 00 00 00 00 00 00

In the full_right area (ID 0xC05), the vehicle sees :
- road : 0x2D
```

# TP2b

## MISRA C
MISRA est un ensemble de bonnes pratiques et de règles à respecter pour du dévelopement en C. Le but est de suivre des règles de sécurité et de fiabilité dans un contexte d'informatique embarquée dans des sytèmes critiques tels que les automobiles, les trains ou les avions. Pour qu'un logiciel soit en accord avec cette norme il doit suivre toutes les règles obligatoires doivent être respectés.


## Identify MISRA non-compliant code

`R 2.7`: Advisory

`R 3.1`: Required

`R 13.4`: Advisory

## Make your code more MISRA compliant
`R 2.7`: Retirer les paramètres non utilisés d'une fonction facilite la lecture du code pour un autre developpeur, il n'y a pas d'information superflus

`R 12.1`: On ajoute des parenthèses aux expressions pour rendre explicite la logique. Un developpeur n'a pas besoin de réfléchir aux priorités opératoires pour connaitre l'ordre des opérations.

`R 17.7`: La valeur retourné par une fonction doit être utilisé ou on doit explicitement la cast en void pour montrer qu'on ne l'utilise pas. Cela permet encore une fois un code plus explicite pour quelqu'un qui nous relis afain de ne laisser passer aucune erreur car des vies sont en jeu et quand je prend le train je n'ai pas à me demander si Dave n'a pas vu qu'il ne récupére pas le résultat super important d'une fonction du programme.

# TP3

## Setup

```bash
sudo modprobe vcan # Load virtual CAN Kernel module
sudo ip link add dev vcan0 type vcan # Add virtual CAN socket called vcan0
sudo ip link add dev vcan1 type vcan # Add virtual CAN socket called vcan0
sudo ip link set up vcan0 # Set vcan0 up
sudo ip link set up vcan1 # Set vcan0 up
```
## 1. Create a simplified OBD2 diagnostic interface