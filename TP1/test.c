#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

// Flag for 29 bits ID 👍
#define CAN_EFF_FLAG 0x80000000U

int main(int argc, char **argv)
{
	int s; 
	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;

	printf("CAN Sockets Demo\r\n");

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}

	strcpy(ifr.ifr_name, "vcan0" );
	ioctl(s, SIOCGIFINDEX, &ifr);

	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return 1;
	}

    // CAN SEND

    // We use the flag to have a 29 bits ID
	frame.can_id = 0x8123 | CAN_EFF_FLAG;
	frame.can_dlc = 8;
	sprintf(frame.data, "Bonjour!");

    for (int i = 0; i < 8; ++i) {

        if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
            perror("Write");
            return 1;
        }

    }

    // CAN RECEIVE/FILTER

    struct can_filter rfilter[1];

    // We listen for every frames with an ID between 0x100 and 0x1FF so we use a filter 👂
	rfilter[0].can_id   = 0x100;
	rfilter[0].can_mask = 0xF00;

	setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, &rfilter, sizeof(rfilter));

    int nbytes;
	nbytes = read(s, &frame, sizeof(struct can_frame));

	if (nbytes < 0) {
		perror("Read");
		return 1;
	}

	printf("0x%03X [%d] ",frame.can_id, frame.can_dlc);

	for (int i = 0; i < frame.can_dlc; i++)
		printf("%02X ",frame.data[i]);

	printf("\r\n");


    // CLOSE
	if (close(s) < 0) {
		perror("Close");
		return 1;
	}

	return 0;
}