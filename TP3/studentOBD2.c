#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

// Flag for 29 bits ID 👍
#define CAN_EFF_FLAG 0x80000000U

int main(int argc, char **argv)
{

    // ####################
    // # BIND VCAN0       #
    // ####################
	int s0; 
	struct sockaddr_can addr0;
	struct ifreq ifr0;

	printf("CAN Sockets Demo\r\n");

	if ((s0 = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}

	strcpy(ifr0.ifr_name, "vcan0" );
	ioctl(s0, SIOCGIFINDEX, &ifr0);

	memset(&addr0, 0, sizeof(addr0));
	addr0.can_family = AF_CAN;
	addr0.can_ifindex = ifr0.ifr_ifindex;

	if (bind(s0, (struct sockaddr *)&addr0, sizeof(addr0)) < 0) {
		perror("Bind");
		return 1;
	}

    // ####################
    // # BIND VCAN1       #
    // ####################
	int s1; 
	struct sockaddr_can addr1;
	struct ifreq ifr1;

	printf("CAN Sockets Demo\r\n");

	if ((s1 = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return 1;
	}

	strcpy(ifr1.ifr_name, "vcan1" );
	ioctl(s1, SIOCGIFINDEX, &ifr1);

	memset(&addr1, 0, sizeof(addr1));
	addr1.can_family = AF_CAN;
	addr1.can_ifindex = ifr1.ifr_ifindex;

	if (bind(s1, (struct sockaddr *)&addr1, sizeof(addr1)) < 0) {
		perror("Bind");
		return 1;
	}



    // ####################
    // # TIMEOUT CONFIG   #
    // ####################


    // set timeout for can1 read to 1s
    struct timeval timeout;      
    timeout.tv_sec = 0;
    timeout.tv_usec = 1;
    setsockopt(s0, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
    setsockopt(s1, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));


    // ####################
    // # FILTER           #
    // ####################

    // We set filters for rpm, speed and throttle that's a secret tool that we will use later :-) 👂

    struct can_filter rpmFilter;
	rpmFilter.can_id   = 0xC06;
	rpmFilter.can_mask = 0xFFF;

    struct can_filter speedFilter;
	speedFilter.can_id   = 0xC07;
	speedFilter.can_mask = 0xFFF;

    struct can_filter throttleFilter;
	throttleFilter.can_id   = 0x321;
	throttleFilter.can_mask = 0xFFF;

    struct can_filter obd2Filter;
	throttleFilter.can_id   = 0x7DF;
	throttleFilter.can_mask = 0xFFF;

    // ####################
    // # MAIN             #
    // ####################

	struct can_frame frame;

    int nbytes;

    int rpm = 0;
    int rpm_raw = 0;
    int speed = 0;
    int throttle = 0;

	
    while (1){

        // ####################
        // # LISTEN FOR VCAN0 #
        // ####################

        // Listen for DATA (rpm, speed, throttle and swag level)

        //Engine RPM
            // ID : 0xC06 DLC : 2 0-1 motor speed 0 rpm (0x00) 5998 rpm (0x6E 0x17)
            // 0C	12	2	Engine RPM	0	16,383.75	rpm

	    setsockopt(s0, SOL_CAN_RAW, CAN_RAW_FILTER, &rpmFilter, sizeof(rpmFilter));
        nbytes = read(s0, &frame, sizeof(struct can_frame));
        if (nbytes > 0) {
            rpm_raw= (frame.data[1]<<8) + frame.data[0];
            rpm = (float) rpm_raw / 0x6e17 * 5998;
            printf("New rpm is: %i \r\n", rpm);
        }

        // Speed
            // ID : 0xC07 DLC : 2 0 vehicle speed 0 km/h (0x00) 214 km/h% (0xD6)
            // 0D	13	1	Vehicle speed	0	255	km/h

	    setsockopt(s0, SOL_CAN_RAW, CAN_RAW_FILTER, &speedFilter, sizeof(speedFilter));
        nbytes = read(s0, &frame, sizeof(struct can_frame));
        if (nbytes > 0) {
            speed = (double) frame.data[0] / 0xD6 * 214;
            printf("New speed is: %i \r\n", speed);
        }


        // Throttle
            // ID : 0x321 DLC : 3 0 throttle 0% (0x00) 100% (0x64)
            // 11	17	1	Throttle position	0	100	%

	    setsockopt(s0, SOL_CAN_RAW, CAN_RAW_FILTER, &throttleFilter, sizeof(throttleFilter));
        nbytes = read(s0, &frame, sizeof(struct can_frame));
        if (nbytes > 0) {
            throttle = frame.data[0];
            printf("New throttle is: %i \r\n", throttle);
        }


        // ####################
        // # LISTEN FOR VCAN1 #
        // ####################

        // Wait request on can1 for 1us
	    setsockopt(s1, SOL_CAN_RAW, CAN_RAW_FILTER, &obd2Filter, sizeof(obd2Filter));
        nbytes = read(s1, &frame, sizeof(struct can_frame));
        if (nbytes > 0) {
            printf("Congrats we received: %d bytes \r\n", nbytes);


            if(frame.data[0] != 0x01){
                printf("This is not a current data request hummm stange mate\r\n");
            }

            switch (frame.data[1]) {
                case 0x0C:
                    // send RPM
                    break;
                case 0x0D:
                    // send speed
                    break;
                case 0x11:
                    // send throttle
                    break;
            }


        //printf("beep boop\r\n");

    }






    // ####################
    // # CLOSE S0 & S1    #
    // ####################

	if (close(s0) < 0 | close(s1) < 0) {
		perror("Close");
		return 1;
	}

	return 0;
}